
(require "lib")
(require "short")

(define opening {"(" "$" "[" ":"})
(define whitesp {" " "\t" "\n"})
(define delim $ append! {"{" "]" ")" "\"" "\\" "#"} $ append opening whitesp)
(define closing {"]" ")" "}"})

#!# (inpexpr stream)
 ! where stream is a (potentially infinite) stream of characters
 ! returns: (cons rest-of-stream value-read)
 !#
(define [inpexpr s]
   : null? s [error "EOF when expression expected"]
  : let \ch (l s)
   : match-any? opening ch [inpstruct s]
   : match-any? whitesp ch [inpexpr $skipws s]
   : = ch "#"
      [case-with #!TODO!#
   : = ch "{" [inplist s]
   : = ch "\"" [inpstr "" $r s]
   : = ch "\\"
      [cons^! \s \v (inpexpr $r s)
         : cons s $cons () v ]
    : inpatom "" s
)

(define [skipws s]
   : match-any? whitesp (l s) [skipws $r s]
    \ s
)

(define [skipcomment s]
   : cons^ \c \rs
      [if (= c "!")
         [skipnested rs]
         : r $remove-while (~= "\n") s
      ]
   : error "EOF while inside comment"
)

(define [skipnested s]
   : cons^ \c \rs
      [ & (= "!" c) (= "#" $l rs)
         [r rs]
      : = "#" c
         [skipnested $skipcomment rs]
      : skipnested rs
      ]
    : error "EOF while inside block comment"
)

(define [inpatom accum s]
   : null? s \accum
   : match-any? delim (l s)
      [cons s $if (= 0 $strlen accum)
                 [error $ strcat "unexpected "
                        $ if (null? s) "EOF" : l s]
                 \ accum
      ]
    : inpatom (strcat accum $l s) (r s)
)

(define [inpstr accum s]
   : cons^ \ch \chs s
      [ = ch "\"" [cons s accum]
      : = ch "\\"
        [let \ch (l chs)
         : inpstr
             (strcat accum $case-with (= ch) {"n""\n" "t""\t" ch})
             $r chs
        ]
      : inpstr (strcat accum ch) chs
      ]
    : error "EOF while inside string"
)

(define [inpstruct s]
  : let \body (/\accum: /\s
      : let \s (skipws s)
       : match-any? closing (l s) [cons s accum]
        : cons^! \s \v (inpexpr s)
           : body (cons accum v) s
    )
  : let \open (l s)
  : let \s (r s)
   : case-with (= open){
     "$"[prefix $r s]
     "("[cons^! \s \v (prefix s)
           : if (= ")" $l s) [cons (r s) v]
                             : error "unmatched ("]
     "["[cons^! \s \v (prefix s)
           : if (= "]" $l s) [cons (r s) $cons () v]
                             : error "unmatched ["]
     ":"[cons^! \s \v (prefix s)
           : cons s $cons () v]
   }
)

